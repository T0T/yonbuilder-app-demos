# yonBuilder移动开发示例集合

## 介绍

YonBuilder移动开发示例集合
记录一些我自己编写的用于展示YonBuilder移动开发技术的示例DEMO源码

[个人博客：https://blog.csdn.net/SMDOUSM](https://blog.csdn.net/SMDOUSM)

## 使用说明

1.  创建新项目：自己创建一个应用项目，并使用YonStudio开发工具通过云端检出的方式将新项目源码检出到本地；
2.  clone源码到项目：复制本仓库中的demo项目源码，替换上面导出的新项目源码（注意：其中config.xml中的appId要使用你上传自己创建的那个项目的appId）
3.  上传源代码：在YonStudio开发工具中鼠标右键点击项目根目录，选择「代码管理」-「上传代码包」，将代码上传云端工作台
4.  添加原生插件：如果demo项目中引用的原生插件，那么需要在云端工作台进入应用，添加对应的插件
5.  添加证书：在新项目的APP证书页面，添加证书（或者选择android的一键创建证书）
6.  版本编译：完成上面的步骤，就可以在移动打包页面编译需要的（测试版、自定义loader、正式版）版本安装包了。

## 示例应用资源目录

### [第一个应用](./demos/第一个应用)

相关教程：[【2023最新】超详细图文保姆级教程](https://blog.csdn.net/SMDOUSM/article/details/130132157)

### [App自定义更新](./demos/App自定义更新)

相关教程：[YonBuilder移动开发基础教程——App版本更新功能开发](https://blog.csdn.net/SMDOUSM/article/details/135983005)

### [多语言国际化](./demos/多语言国际化)

相关教程：[App开发——国际化多语言的实现](https://blog.csdn.net/SMDOUSM/article/details/135681300)

### [本地文件选择和展示](./demos/filesLoadDemo)

项目需要添加插件：fileBrowser






