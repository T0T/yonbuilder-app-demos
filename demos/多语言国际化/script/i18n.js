(function(global){
    var i18nMapData =  {
        '多语言国际化示例': {
            'zh-CN': '多语言国际化示例',
            'en-US': 'i18n Demo'
        },
        '人物简介': {
            'zh-CN': '人物简介',
            'en-US': 'profile'
        },
        '姓名': {
            'zh-CN': '姓名',
            'en-US': 'name'
        },
        '张三': {
            'zh-CN': '张三',
            'en-US': 'three'
        },
        '年龄': {
            'zh-CN': '年龄',
            'en-US': 'age'
        },
    }
    
    /**
     * 存储国际化语言的判断标识
     * @param {String} i18nSign 
     */
    function setI18n(i18nSign) {
        // 新的系统语言标识
        var i18nStr = i18nSign ? i18nSign : api.language;
        // 本地缓存
        api.setPrefs({
            key: 'i18n',
            value: i18nStr
        });
        // 发送同步通知，用于一些已经初始化的页面进行动态切换(按业务需要，非必须)
        api.sendEvent({
            name:'i18nChange',
            extra: {
                i18nSign: i18nStr
            }
        })
    }
    
    /**
     * 读取国际化语言的判断标识（采用同步策略）
     * @returns 
     */
    function getI18n() {
        var i18nStr = api.getPrefs({
            sync: true,
            key: 'i18n'
        });
        if(!i18nStr) {
            i18nStr = api.language;
            setI18n(i18nStr);
        }
        return i18nStr;
    }
    
    /**
     * 添加i18n改变的页面监听
     * @param {Function} callBack 触发事件后的回调函数
     */
    
    function addI18nChangeListener(callBack) {
        api.addEventListener({
            name:'i18nChange'
        }, function(ret){
            if(typeof callBack === 'function') {
                callBack(ret.value);
            }
        })
    }
    
    /**
     * 返回国际化的对应文本内容
     * @param {String} wordStr 内容元数据
     * @returns {String} 翻译后的映射文本
     */
    function parseWord(wordStr) {
        if(!wordStr) return '';
        var wordObj = i18nMapData[wordStr];
        if(!wordObj) return wordStr;
        var i18nKey = getI18n();
        var i18nStr = wordObj[i18nKey];
        if(!i18nStr) return wordStr;
        return i18nStr;
    }

    global.setI18n = setI18n;
    global.getI18n = getI18n;
    global.addI18nChangeListener = addI18nChangeListener;
    global.parseWord = parseWord;
})(window)
